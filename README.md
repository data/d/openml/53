# OpenML dataset: heart-statlog

https://www.openml.org/d/53

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

This database contains 13 attributes (which have been extracted from
 a larger set of 75)       
   
 
 
 Attribute Information:
 ------------------------
       -- 1. age       
       -- 2. sex       
       -- 3. chest pain type  (4 values)       
       -- 4. resting blood pressure  
       -- 5. serum cholestoral in mg/dl      
       -- 6. fasting blood sugar > 120 mg/dl       
       -- 7. resting electrocardiographic results  (values 0,1,2) 
       -- 8. maximum heart rate achieved  
       -- 9. exercise induced angina    
       -- 10. oldpeak = ST depression induced by exercise relative to rest   
       -- 11. the slope of the peak exercise ST segment     
       -- 12. number of major vessels (0-3) colored by flourosopy        
       -- 13.  thal: 3 = normal; 6 = fixed defect; 7 = reversable defect     
 
 Attributes types
 -----------------
 
 Real: 1,4,5,8,10,12
 Ordered:11,
 Binary: 2,6,9
 Nominal:7,3,13
 
 Variable to be predicted
 ------------------------
 Absence (1) or presence (2) of heart disease
 
 Cost Matrix
 
          abse  pres
 absence   0     1
 presence  5     0
 
 where the rows represent the true values and the columns the predicted.
 
 No missing values.
 
 270 observations




 Relabeled values in attribute class
    From: 1                       To: absent              
    From: 2                       To: present

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/53) of an [OpenML dataset](https://www.openml.org/d/53). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/53/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/53/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/53/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

